<?php

$required_extensions = [
	'Core',
	'bcmath',
	'ctype',
	'date',
	'filter',
	'hash',
	'iconv',
	'json',
	'mcrypt',
	'SPL',
	'pcre',
	'Reflection',
	'session',
	'standard',
	'mysqlnd',
	'tokenizer',
	'zip',
	'zlib',
	'libxml',
	'dom',
	'PDO',
	'bz2',
	'SimpleXML',
	'xml',
	'xmlreader',
	'xmlwriter',
	'openssl',
	'curl',
	'fileinfo',
	'gd',
	'intl',
	'imap',
	'ldap',
	'mbstring',
	'exif',
	'mysqli',
	'Phar',
	'pdo_mysql',
	'pdo_sqlite',
	'soap',
	'sockets',
	'sqlite3',
	'xmlrpc',
	'xsl',
	/*
	'gmp',
	'calendar',
	'gettext',
	'wddx',
	*/
];


$extensions 		= get_loaded_extensions();
$missing_extensions = array_diff($required_extensions,$extensions);

echo "Loaded PHP extensions:\n\t".implode("\n\t",$extensions);

if(count($missing_extensions)>0){
	throw new Exception("Required extension are not found:\n".implode("\n",$missing_extensions)); 
}

exit(0);